﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TCoreService.Filters;
using TCoreService.Models;
using TCoreService.Repositories;
using TCoreService.Services;

namespace TCoreService.Controllers
{
    [Route("api/[controller]")]
    public class ItemController : Controller
    {
        private readonly IItemService _service;

        public ItemController(IItemService service)
        {
            _service = service;
        }

        // GET api/item
        [HttpGet]
        public async Task<IEnumerable<Item>> Get()
        {
            return await _service.GetAllAsync();
        }

        // GET api/item/5
        [HttpGet("{id}")]
        public async Task<Item> Get(Guid id)
        {
            return await _service.GetByIdAsync(id);
        }

        // POST api/item
        [HttpPost]
        [ModelStateValidationFilter]
        public async Task<IActionResult> Post([FromBody]Item item)
        {
            await _service.AddAsync(item);
            return Ok();
        }

        // PUT api/item
        [HttpPut]
        [ModelStateValidationFilter]
        public async Task<IActionResult> Put([FromBody]Item item)
        {
            await _service.UpdateAsync(item);
            return Ok();
        }

        // DELETE api/item
        [HttpDelete]
        public async Task Delete(Guid id)
        {
            await _service.DeleteAsync(id);
        }
    }
}
