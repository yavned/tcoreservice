﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using TCoreService.Models;

namespace TCoreService.Entities
{
    public class ApplicationDbContext : IdentityDbContext
    {
        private readonly string connectionString; 

        public ApplicationDbContext(IConfiguration configuration){
            connectionString = configuration.GetConnectionString("DefaultConnection");
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //some extra table
            //modelBuilder.Entity<Item>()
            //    .Property(x => x.id)
            //    .HasDefaultValue(Guid.NewGuid());                
            modelBuilder.Entity<Item>().HasKey(x => x.id);
        }
    }
}