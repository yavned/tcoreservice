using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using TCoreService.Models;

namespace TCoreService.Entities
{
    public class TCoreDbContext : DbContext
    {
        private readonly string connectionString;

        public TCoreDbContext(DbContextOptions<TCoreDbContext> options, IConfiguration configuration)
            : base(options)
        {
            connectionString = configuration.GetConnectionString("DefaultConnection");
        }

        public DbSet<Item> Item { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(connectionString); //alternative to use services.AddDbContext<TCoreDbContext>(c => o.UseMySql(connectionString)) in Startup
        }
    }
}