﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TCoreService.Models
{
    public class Item
    {
        //[Key]
        public Guid? id { get; set; }
        public string name { get; set; }
        public float price { get; set; }
        public string description { get; set; }
    }
}
