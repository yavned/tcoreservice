**ASP.NET Core 2 Web API 2.0.5**

Just a template with some features plugged in and configured. To be used as a start point.

## Features & Libs

1. Serilog (Console, File). 
Configured using this: [SimpleWebSample](https://github.com/serilog/serilog-aspnetcore/tree/dev/samples/SimpleWebSample) 
and this: [Serilog.AspNetCore](https://github.com/serilog/serilog-aspnetcore)
2. Swagger.
3. EntityFrameworkCore.
4. MySql data provider.

---

## Functionality

1. [Asp.NET Core 2.0 WebApi JWT Authentication with Identity & MySQL](https://medium.com/@lugrugzo/asp-net-core-2-0-webapi-jwt-authentication-with-identity-mysql-3698eeba6ff8)
Register new user.
2. User Login/Logout.
3. Some CRUD operations.
4. Global error logging.