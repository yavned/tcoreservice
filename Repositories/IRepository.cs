using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TCoreService.Repositories
{
    public interface IRepository<T> where T : class
    {
        // Marks an entity as new
        Task AddAsync(T entity);
        // Marks an entity as modified
        Task UpdateAsync(T entity);
        // Marks an entity to be removed
        Task DeleteAsync(Guid id);
        // Get an entity by int id
        Task<T> GetByIdAsync(Guid id);
        // Gets all entities of type T
        Task<IEnumerable<T>> GetAllAsync();
    }
}