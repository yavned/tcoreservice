using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using TCoreService.Entities;
using TCoreService.Models;

namespace TCoreService.Repositories
{
    public class ItemRepository : IRepository<Item>
    {
        private readonly TCoreDbContext _context;

        public ItemRepository(TCoreDbContext context)
        {
            _context = context;
        }

        public async Task AddAsync(Item entity)
        {
            _context.Item.Add(entity);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var item = new Item { id = id };
            _context.Entry(item).State = EntityState.Deleted;
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Item>> GetAllAsync()
        {
            return _context.Set<Item>();
        }

        public async Task<Item> GetByIdAsync(Guid id)
        {
           return await _context.Item.FindAsync(id);
        }

        public async Task UpdateAsync(Item entity)
        {
            var item = await _context.Item.SingleOrDefaultAsync(x => x.id == entity.id);
            if (item != null)
            {
                item.name = entity.name;
                item.price = entity.price;
                item.description = entity.description;
                await _context.SaveChangesAsync();
            }
        }
    }
}