using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TCoreService.Models;

namespace TCoreService.Services
{
    public interface IItemService
    {
        Task AddAsync(Item item);
        Task<Item> GetByIdAsync(Guid id);
        Task<IEnumerable<Item>> GetAllAsync();
        Task DeleteAsync(Guid id);
        Task UpdateAsync(Item item);
    }
}