using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TCoreService.Models;
using TCoreService.Repositories;

namespace TCoreService.Services
{
    public class ItemService : IItemService
    {
        private readonly IRepository<Item> _repository;

        public ItemService(IRepository<Item> repository)
        {
            _repository = repository;
        }

        public async Task AddAsync(Item item)
        {
            item.id = Guid.NewGuid();
            await _repository.AddAsync(item);
        }

        public async Task DeleteAsync(Guid id)
        {
            await _repository.DeleteAsync(id);
        }

        public async Task<IEnumerable<Item>> GetAllAsync()
        {
            return await _repository.GetAllAsync();
        }

        public async Task<Item> GetByIdAsync(Guid id)
        {
            return await _repository.GetByIdAsync(id);
        }

        public async Task UpdateAsync(Item item)
        {
            await _repository.UpdateAsync(item);
        }
    }
}